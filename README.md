# python-pgi

Pure Python GObject Introspection Bindings. Needed for gi with pypy

https://pypi.python.org/pypi/pgi

<br>

How to clone this repository:

```
git clone https://gitlab.com/azul4/content/multimedia/tartube/python-pgi.git
```

